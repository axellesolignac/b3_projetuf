# Gestion des stocks

Installer et déployer l'API
=

Prérequis
-
* cloner le projet :
	`git clone https://gitlab.com/axellesolignac/b3_projetuf.git`

* installer python (v3 ou plus) :

  

	**WINDOWS** -> https://www.python.org/downloads/

  

	**LINUX** -> `sudo apt-get install python3 python3-pip python3-venv`

  

* installer django, djangorestframework, djangorestframework-simplejwt, drf-yasg :

  

	**WINDOWS** -> <br>`py -m pip install Django`<br>`py -m pip install djangorestframework`<br>`py -m pip install djangorestframework-simplejwt`<br>`py -m pip install -U drf-yasg`

  

	**LINUX** il faut créer un environnement virtuel -> <br>`python3 -m venv env`<br>`source env/bin/activate`<br>`pip install django`<br>`pip install djangorestframework`<br>`pip install djangorestframework-simplejwt`<br>`pip install -U drf-yasg`

* mettre le dossier Script (`C:\Users\Username\AppData\Local\Programs\Python\Python39\Scripts\`) dans la variable d'environnement PATH

Déploiement
-
* Se placer dans le dossier du projet

* Executer les commandes suivantes :


	**WINDOWS** -> <br>`py manage.py makemigrations`<br>`py manage.py migrate`

	**LINUX** -> <br>`python3 manage.py makemigrations`<br>`python3 manage.py migrate`

* Démarrer le serveur :

	**WINDOWS** -> `py manage.py runserver`

	**LINUX** -> `python3 manage.py runserver`

	L'adresse par défault est 127.0.0.1:8000/ 

### Chemin :
Produit :
```
<url>/produit                 -> Liste de tous les produits
<url>/produit/<int:IDProduit> -> Détail d'un produit
```

Stock :
```
<url>/stock                 -> Liste de tous les stocks
<url>/stock/<int:IDStock>   -> Détail d'un stock
```

Client :
```
<url>/client                 -> Liste de tous les clients
<url>/client/<int:IDClient>  -> Détail d'un client
```

Recette :
```
<url>/recette                  -> Liste de toutes les recettes
<url>/recette/<int:IDRecette>  -> Détail d'une recette
```

Vente :
```
<url>/vente                 -> Liste de toutes les vente s
<url>/vente/<int:IDVente>   -> Détail d'une vente 
```
