from django.contrib import admin
from django.urls import path

from rest_framework_simplejwt import views as jwt_views
from rest_framework import permissions

from API_Pizzeria.views.produit import *
from API_Pizzeria.views.stock import *
from API_Pizzeria.views.client import *
from API_Pizzeria.views.recette import *
from API_Pizzeria.views.vente import *

urlpatterns = [
    path('admin/', admin.site.urls),

    # path for products
    path('produit', ProduitView.as_view(), name='produits_list'),
    path('produit/<int:produitID>', ProduitDetailView.as_view(), name='produit_detail'),

    # path for stock
    path('stock', StockView.as_view(), name='stocks_list'),
    path('stock/<int:stockID>', StockDetailView.as_view(), name='stock_detail'),

    # path for client
    path('client', ClientView.as_view(), name='clients_list'),
    path('client/<int:clientID>', ClientDetailView.as_view(), name='client_detail'),

    # path for recette
    path('recette', RecetteView.as_view(), name='recettes_list'),
    path('recette/<int:recetteID>', RecetteDetailView.as_view(), name='recette_detail'),

    # path for vente
    path('vente', VenteView.as_view(), name='ventes_list'),
    path('vente/<int:venteID>', VenteDetailView.as_view(), name='vente_detail')
]
