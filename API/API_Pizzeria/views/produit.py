from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from ..models import Produit
from ..serializers.produit import ProduitSerializer


# Définition du retour de L'API lors d'un appel à l'url <url>/produit, ici renvoi une liste
# complète tous les produits. Les champs renvoyés sont renseignés dans le sérializer correspondant.
class ProduitView(ListCreateAPIView):
    serializer_class = ProduitSerializer
    queryset = Produit.objects.all()


# Définition du retour de l'API lors d'un appel à l'url <url>/produit/<int:IDProduit>, permet de renvoyer
# les informations d'un produit spécifique.
class ProduitDetailView(RetrieveUpdateDestroyAPIView):
    lookup_url_kwarg = 'produitID'
    serializer_class = ProduitSerializer
    queryset = Produit.objects.all()
