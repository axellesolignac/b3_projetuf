from django.db import transaction
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response

from ..models import Recette, Produit, ProduitRecette
from ..serializers.recette import RecetteSerializer


# Définition du retour de L'API lors d'un appel à l'url <url>/recette, ici renvoi une liste
# complète toutes les recettes. Les champs renvoyés sont renseignés dans le sérializer correspondant.
class RecetteView(ListCreateAPIView):
    serializer_class = RecetteSerializer
    queryset = Recette.objects.all()

    # Une recette étant composé de plusieurs produits, nous sommes obligé de surcharger la méthode
    # post pour pouvoir envoyer une liste qui fait le lien entre les champs d'une table et d'une autre
    def post(self, request, *args, **kwargs):
        intitule = request.data.get('intitule')
        ingredients = request.data.get('ingredients')
        prix = float(request.data.get('prix'))

        recette = Recette.objects.create(intitule=intitule, prix=prix)

        if ingredients:
            for ingredient in ingredients:
                IDProduit = ingredient.get('IDProduit')
                quantite = float(ingredient.get('quantite'))

                produit = Produit.objects.get(id=IDProduit)
                ProduitRecette.objects.create(recette=recette, produit=produit, quantite=quantite)

        data = self.get_serializer(recette).data

        return Response(data, status=201)


class RecetteDetailView(RetrieveUpdateDestroyAPIView):
    lookup_url_kwarg = 'recetteID'
    serializer_class = RecetteSerializer
    queryset = Recette.objects.all()
