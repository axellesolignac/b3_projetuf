from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from ..models import Client
from ..serializers.client import ClientSerializer


# Définition du retour de L'API lors d'un appel à l'url <url>/client, ici renvoi une liste
# complète tous les clients. Les champs renvoyés sont renseignés dans le sérializer correspondant
class ClientView(ListCreateAPIView):

    # get du serializer pour renvoyer les champs créés
    serializer_class = ClientSerializer

    # sélection de tout le contenu de l'objet
    queryset = Client.objects.all()


# Définition du retour de l'API lors d'un appel à l'url <url>/client/<int:IDClient>, qui renvoi
# les infos du client dont l'Id a été spécifié
class ClientDetailView(RetrieveUpdateDestroyAPIView):
    lookup_url_kwarg = 'clientID'
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
