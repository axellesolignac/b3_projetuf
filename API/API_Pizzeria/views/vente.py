from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response

from ..models import Vente, Recette, Client, VenteRecette, DateDimension
from ..serializers.vente import VenteSerializer


class VenteView(ListCreateAPIView):
    serializer_class = VenteSerializer
    queryset = Vente.objects.all()

    def post(self, request, *args, **kwargs):
        IDClient = request.data.get('client')
        recettes = request.data.get('recettes')
        date = request.data.get('date')
        heure = request.data.get('heure')

        client = Client.objects.get(id=IDClient)

        datedimmension = DateDimension.objects.get(CalendarDateInterval=date)

        vente = Vente.objects.create(client=client, date=datedimmension, heure=heure)

        for recetteDetail in recettes:
            recette = Recette.objects.get(id=recetteDetail.get('IDRecette'))
            quantite = recetteDetail.get('quantite')

            VenteRecette.objects.create(vente=vente, recette=recette, quantite=quantite)

        data = self.get_serializer(vente).data

        return Response(data, status=201)


class VenteDetailView(RetrieveUpdateDestroyAPIView):
    lookup_url_kwarg = 'venteID'
    serializer_class = VenteSerializer
    queryset = Vente.objects.all()
