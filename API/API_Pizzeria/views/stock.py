from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from ..models import Stock
from ..serializers.stock import StockSerializer


class StockView(ListCreateAPIView):
    serializer_class = StockSerializer
    queryset = Stock.objects.all()


class StockDetailView(RetrieveUpdateDestroyAPIView):
    lookup_url_kwarg = 'stockID'
    serializer_class = StockSerializer
    queryset = Stock.objects.all()
