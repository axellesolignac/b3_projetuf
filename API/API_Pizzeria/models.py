from django.contrib.auth.models import User
from django.db import models

##############################
# Définition des classes, c'est ici que l'on dertermine
# Les champs de chaque classes lors d'un post
##############################

# modèle pour Produit
class Produit(models.Model):
    intitule = models.CharField(max_length=255, null=True, blank=False)
    prix = models.FloatField(null=True, blank=False)
    datePeremption = models.DateField(null=True, blank=False)
    dateLivre = models.DateField(null=True, blank=False)
    quantite = models.FloatField(null=True, blank=False)
    codeBarre = models.IntegerField(null=True, blank=False)
    origine = models.CharField(max_length=255, null=True, blank=False)

    def __str__(self):
        return f"{self.codeBarre} {self.intitule} {self.prix} "


# Modèle pour Stock
class Stock(models.Model):
    IDProduit = models.ForeignKey(Produit, models.SET_NULL, null=True, blank=False)
    quantiteProduit = models.FloatField(null=True, blank=False)

    def __str__(self):
        return f"{self.IDProduit} {self.quantiteProduit}"


# Modèle pour Client
class Client(models.Model):
    nom = models.CharField(max_length=255, null=True, blank=False)
    prenom = models.CharField(max_length=255, null=True, blank=False)
    adresse = models.CharField(max_length=255, null=True, blank=False)
    telephone_1 = models.IntegerField(null=True, blank=False)
    telephone_2 = models.IntegerField(null=True, blank=False)

    def __str__(self):
        return f"{self.nom} {self.prenom}"


# Modèle pour Recette
class Recette(models.Model):
    intitule = models.CharField(max_length=255, null=True, blank=False)
    prix = models.FloatField(null=True, blank=False)
    compose = models.ManyToManyField(Produit, through='ProduitRecette', null=True, blank=False)

    def __str__(self):
        return f"{self.intitule}"


# Ici on créé un modèle faisant l'intermédiaire entre Produit et Recette
# Ce qui permet de dynamiquement mapper des listes
class ProduitRecette(models.Model):
    recette = models.ForeignKey(Recette, on_delete=models.CASCADE, blank=False)
    produit = models.ForeignKey(Produit, on_delete=models.CASCADE, blank=False)
    quantite = models.FloatField(null=True, blank=False)


# Création de la dimmension Date qui sera utile notemment lors de la creation du BI
class DateDimension(models.Model):
    CalendarDateInterval = models.DateField(blank=False, primary_key=True, default='2021-01-01')
    DayNumber = models.IntegerField(null=True, blank=False)
    DayOfWeek = models.CharField(null=True, blank=False, max_length=255)
    DayOfMonth = models.CharField(null=True, blank=False, max_length=255)
    IsWeekend = models.SmallIntegerField(null=True, blank=False)
    IsWeekday = models.SmallIntegerField(null=True, blank=False)
    MonthNumber = models.IntegerField(null=True, blank=False)
    MonthName = models.CharField(null=True, blank=False, max_length=255)
    YearNumber = models.IntegerField(null=True, blank=False)


# Modèle pour Vente
class Vente(models.Model):
    recettes = models.ManyToManyField(Recette, through='VenteRecette', blank=False)
    client = models.ForeignKey(Client, on_delete=models.CASCADE, blank=False)
    date = models.ForeignKey(DateDimension, blank=False, on_delete=models.CASCADE, null=True)
    heure = models.TimeField(null=True, blank=False)

    def __str__(self):
        return f"{self.client}"


# Modèle pour une table intermédiaire entre Vente et Recette
class VenteRecette(models.Model):
    recette = models.ForeignKey(Recette, on_delete=models.CASCADE, blank=False)
    vente = models.ForeignKey(Vente, on_delete=models.CASCADE, blank=False)
    quantite = models.IntegerField(null=True, blank=False, default=1)
