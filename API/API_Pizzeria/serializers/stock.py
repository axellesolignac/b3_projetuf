from rest_framework import serializers
from ..models import Stock


# Définition du serializer pour Stock
class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock
        fields = ['id', 'IDProduit', 'quantiteProduit']
