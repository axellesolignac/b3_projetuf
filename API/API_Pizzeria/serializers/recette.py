from rest_framework import serializers

from ..models import Recette, ProduitRecette


# Définition du serializer pour la table intermédiaire RecettePr
class RecetteProduitSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProduitRecette
        fields = ['produit', 'quantite']


# Définition du serializer pour Recette
class RecetteSerializer(serializers.ModelSerializer):
    compose = RecetteProduitSerializer(source="produitrecette_set", many=True)

    class Meta:
        model = Recette
        fields = ['id', 'intitule', 'prix', 'compose']
