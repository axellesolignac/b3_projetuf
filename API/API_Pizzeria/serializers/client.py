from rest_framework import serializers
from ..models import Client


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'nom', 'prenom', 'adresse', 'telephone_1', 'telephone_2']
