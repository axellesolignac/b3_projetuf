from rest_framework import serializers
from ..models import Produit


class ProduitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Produit
        fields = ['id', 'intitule', 'prix', 'datePeremption', 'dateLivre', 'quantite', 'codeBarre', 'origine']
