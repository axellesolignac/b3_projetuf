from rest_framework import serializers

from ..models import Vente, VenteRecette


# Définition du serializer pour la table intermédiaire VenteRecette
class VenteRecetteSerializer(serializers.ModelSerializer):
    IDRecette = serializers.IntegerField(source='recette.id')

    class Meta:
        model = VenteRecette
        fields = ['IDRecette', 'quantite']


# Définition du serializer pour Vente
class VenteSerializer(serializers.ModelSerializer):
    recettes = VenteRecetteSerializer(source='venterecette_set', many=True)

    class Meta:
        model = Vente
        fields = ['id', 'client', 'recettes', 'date', 'heure']
