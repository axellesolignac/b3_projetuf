from django.contrib import admin
from .models import *

admin.site.register(Produit)
admin.site.register(Stock)
admin.site.register(Client)
admin.site.register(Recette)
admin.site.register(ProduitRecette)
admin.site.register(VenteRecette)
admin.site.register(Vente)
