from django.apps import AppConfig


class ApiPizzeriaConfig(AppConfig):
    name = 'API_Pizzeria'
