# UiEspigaou

##Installation

Prérequis
-
* cloner le projet :
	`git clone https://gitlab.com/axellesolignac/b3_projetuf.git`

* Accès terminal pour lancer le projet :

**Invite de commande** -> cd  <chemin où vous avez clone le projet>/b3_projetuf/UI

**Git Bash** -> Aller dans le dossier UI avec votre explorateur de fichier puis faites click droit puis git Bash

* Lancer le server local:

Dans le terminal lancer -> 'ng serve -o'

Cette commande permet d'ouvrir dans votre navigateur le projet.