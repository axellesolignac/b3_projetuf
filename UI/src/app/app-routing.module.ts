import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DashboardComponent} from 'src/app/dashboard/dashboard.component';
import {CustomerComponent} from 'src/app/customer/customer.component';
import {ProductComponent} from 'src/app/product/product.component';
import {SaleComponent} from 'src/app/sale/sale.component';
import { RecipeComponent } from './recipe/recipe.component';
import { StockComponent } from './stock/stock.component';


const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'customer', component: CustomerComponent },
  { path: 'product', component: ProductComponent },
  { path: 'sale', component: SaleComponent },
  { path: 'recipe', component: RecipeComponent },
  { path: 'stock', component: StockComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }