import { Component, OnInit, Inject } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Customer} from 'src/app/template/classes/customers';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css']
})
export class EditCustomerComponent implements OnInit {
  
  Editform;
  dataClient: any = [];
  id: number;

  constructor(private api: ApiService, public dialog: MatDialog,
  @Inject(MAT_DIALOG_DATA) public data: {
    id: number;
  }) { 
    this.id = data.id;
  }
  ngOnInit(): void {
    // //Affiche tous les clients
    this.api.getClientId(this.id).subscribe(
      data=> {
        this.dataClient = data;
    });
   
    // Formulaire pour l'envoi des data à l'api
    this.Editform = new FormGroup({
      nom: new FormControl(this.dataClient.nom),
      prenom: new FormControl(this.dataClient.prenom),
      adresse: new FormControl(this.dataClient.adresse),
      telephone_1: new FormControl(this.dataClient.telephone_1),
      telephone_2: new FormControl(this.dataClient.telephone_2),
    });
  }

  // Envoi les data en format json
  onSubmit(nom,prenom,adresse,tel1,tel2){
    this.Editform.setValue({nom: nom, prenom: prenom, adresse: adresse, telephone_1: tel1, telephone_2: tel2 });
    this.api.putClient(this.id,this.Editform.value).subscribe();
  }

}
