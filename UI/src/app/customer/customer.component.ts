import { Component, OnInit } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Customer} from 'src/app/template/classes/customers';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material/dialog';
import {AddCustomerComponent} from 'src/app/customer/add-customer/add-customer.component';
import {EditCustomerComponent} from 'src/app/customer/edit-customer/edit-customer.component'

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  firstname: string;
  Addform;
  dataClient: any = [];
  // Variables pour la table
  dataSource = new MatTableDataSource<Customer>();
  displayedColumns: string[] = ['id', 'nom', 'prenom', 'adresse', 'telephone_1', 'telephone_2','Actions'];

  constructor(private api: ApiService, public dialog: MatDialog) {}

  ngOnInit(): void {
    //Affiche tous les clients
    this.api.getClient().subscribe(
      data=> {
        this.dataClient = data;
        this.dataSource = new MatTableDataSource(this.dataClient);
    });
  }

  // Ouvre formulaire d'ajout
  openAdd(){
    const dialogRef = this.dialog.open(AddCustomerComponent);
    dialogRef.afterClosed().subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Ouvre formulaire de modification en passant l'id du client
  openEdit(id){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id : id
    }
    const dialogRef = this.dialog.open(EditCustomerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Supprime le client de la ligne choisie
  onDelete(id){
    this.api.deleteClient(id).subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Fonction pour la recherche d'un client
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
