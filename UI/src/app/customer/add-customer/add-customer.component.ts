import { Component, OnInit } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Customer} from 'src/app/template/classes/customers';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent implements OnInit {

  Addform;

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    // Formulaire pour l'envoi des data à l'api
    this.Addform = new FormGroup({
      nom: new FormControl(),
      prenom: new FormControl(),
      adresse: new FormControl(),
      telephone_1: new FormControl(),
      telephone_2: new FormControl(),
    });
  }

  // Envoi les data en format json
  onSubmit(data){
    this.api.postClient(data).subscribe();
  }

}
