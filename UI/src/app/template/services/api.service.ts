import { Injectable } from '@angular/core';
import { HttpClient, JsonpClientBackend } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {Customer} from 'src/app/template/classes/customers'
import {environment} from 'src/environments/environment';
import {HttpEvent, HttpHandler, HttpInterceptor,HttpRequest,HttpResponse,HttpErrorResponse} from '@angular/common/http';
import { Recipe } from '../classes/Recipes';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // Appel de l'import HttpClient qui nous permet de faire des requêtes via une url à l'API
  constructor(private http: HttpClient) { }

  /************************** CLIENT **************************/
  //Affiche tous les clients
  getClient(){
    return this.http.get(environment.urlApi+'client').pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  // Affiche les data d'un client
  getClientId(id){
    return this.http.get(environment.urlApi+'client/'+id).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  // Création nouveau client
  postClient(newClient){
    return this.http.post<Customer>(environment.urlApi+'client',newClient).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  //Modification data client précis
  putClient(id, editClient){
    return this.http.put<Customer>(environment.urlApi+'client/'+id,editClient).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  // Suppression d'un client
  deleteClient(id){
    return this.http.delete(environment.urlApi+'client/'+id).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

/************************** PRODUIT **************************/
 //Affiche tous les produits
  getProduct(){
    return this.http.get(environment.urlApi+'produit').pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  // Affiche les data d'un produit
  getProductId(id){
    return this.http.get(environment.urlApi+'produit/'+id).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  // Création nouveau produit
  postProduct(newProduct){
    return this.http.post<Customer>(environment.urlApi+'produit',newProduct).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  //Modification data produit précis
  putProduct(id, editProduct){
    return this.http.put<Customer>(environment.urlApi+'produit/'+id,editProduct).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  // Suppression d'un produit
  deleteProduct(id){
    return this.http.delete(environment.urlApi+'produit/'+id).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  /************************** VENTE **************************/
   //Affiche toutes les ventes
   getSale(){
    return this.http.get(environment.urlApi+'vente').pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  // Affiche les data d'une vente
  getSaleId(id){
    return this.http.get(environment.urlApi+'vente/'+id).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  // Création nouvelle vente
  postSale(newSale){
    return this.http.post<Customer>(environment.urlApi+'vente',newSale).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  //Modification data vente précise
  putSale(id, editSale){
    return this.http.put<Customer>(environment.urlApi+'vente/'+id,editSale).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  // Suppression d'une vente
  deleteSale(id){
    return this.http.delete(environment.urlApi+'vente/'+id).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  /************************** RECETTE **************************/
    //Affiche toutes les recettes
    getRecipe(): Observable<Recipe[]>{
    return this.http.get<Recipe[]>(environment.urlApi+'recette').pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  // Affiche les data d'une recette
  getRecipeId(id): Observable<Recipe[]>{
    return this.http.get<Recipe[]>(environment.urlApi+'recette/'+id).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  // Création nouvelle recette
  postRecipe(newRecipe){
    return this.http.post<Recipe>(environment.urlApi+'recette',newRecipe).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  //Modification data recette précise
  putRecipe(id, editRecipe){
    return this.http.put<Recipe>(environment.urlApi+'recette/'+id,editRecipe).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

  // Suppression d'une recette
  deleteRecipe(id){
    return this.http.delete(environment.urlApi+'recette/'+id).pipe(
      catchError(error=> {
        return throwError(error.message)
      })
    );
  }

/************************** STOCK **************************/
 //Affiche tous les stocks
 getStock(){
  return this.http.get(environment.urlApi+'stock').pipe(
    catchError(error=> {
      return throwError(error.message)
    })
  );
}

// Affiche les data d'un stock
getStockId(id){
  return this.http.get(environment.urlApi+'stock/'+id).pipe(
    catchError(error=> {
      return throwError(error.message)
    })
  );
}

// Création nouveau stock
postStock(newStock){
  return this.http.post<Customer>(environment.urlApi+'stock',newStock).pipe(
    catchError(error=> {
      return throwError(error.message)
    })
  );
}

//Modification data stock précis
putStock(id, editStock){
  return this.http.put<Customer>(environment.urlApi+'stock/'+id,editStock).pipe(
    catchError(error=> {
      return throwError(error.message)
    })
  );
}

// Suppression d'un stock
deleteStock(id){
  return this.http.delete(environment.urlApi+'stock/'+id).pipe(
    catchError(error=> {
      return throwError(error.message)
    })
  );
}
}
