export interface Product {
    intitule: string;
    prix: number;
    datePeremption: number;
    dateLivre: number;
    quantite: number;
    codeBarre: number;
    origine: string;
 }