export interface Compose {
    produit: number;
    quantite: number;
 }

export class Recipe {
    id: number;
    intitule: string;
    prix: number;
    compose: 
        {
            produit: number;
            quantite: number;
         }
 }