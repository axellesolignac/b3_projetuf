export interface Customer {
   id: number;
   nom: string;
   prenom: string;
   adresse: string;
   telephone_1: number;
   telephone_2: number; 
}