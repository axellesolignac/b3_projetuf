import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ApiService} from 'src/app/template/services/api.service';
import {Customer} from 'src/app/template/classes/customers';
import {HttpClient} from "@angular/common/http";
import { Observable } from 'rxjs';

@Component({
  selector: 'app-customer-choice',
  templateUrl: './customer-choice.component.html',
  styleUrls: ['./customer-choice.component.css']
})
export class CustomerChoiceComponent implements OnInit {

  dataClient: any = [];
  dataSource = new MatTableDataSource<Customer>();
  displayedColumns: string[] = ['nom', 'prenom', 'adresse'];

  constructor(private api: ApiService){}

  ngOnInit(){
    this.api.getClient().subscribe(
      data=> { 
        this.dataClient = data;
        this.dataSource = new MatTableDataSource(this.dataClient);
    });
  }

  getCustomer(){
    this.api.deleteClient(this.dataClient[2].id).subscribe(); //delete user avec id = 3
    // this.dataClient.forEach(element => {
    //   console.log(element.id); // récupération id client
    // });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
