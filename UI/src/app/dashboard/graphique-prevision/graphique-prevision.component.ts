import { Component, OnInit } from '@angular/core';
import { RadialChartOptions, ChartDataSets, ChartType } from 'chart.js';
import { Label, Color } from 'ng2-charts';

@Component({
  selector: 'app-graphique-prevision',
  templateUrl: './graphique-prevision.component.html',
  styleUrls: ['./graphique-prevision.component.css']
})
export class GraphiquePrevisionComponent {

  public radarChartOptions: RadialChartOptions = {
    responsive: true,
  };
  public radarChartLabels: Label[] = ['Fromage', 'Royale', 'Anchois',
    'Jambon', 'Margherita', '3 Fromages', 'Chorizo'];

  public radarChartData: ChartDataSets[] = [
    { data: [0, 1, 2, 3, 4, 5, 6], label: 'Quantité de ventes par recettes' }
  ];
  public radarChartType: ChartType = 'radar';
  public radarChartColors: Color[] = [
    { backgroundColor: 'green' },
  ]

}
