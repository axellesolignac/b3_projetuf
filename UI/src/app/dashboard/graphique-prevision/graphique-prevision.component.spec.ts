import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphiquePrevisionComponent } from './graphique-prevision.component';

describe('GraphiquePrevisionComponent', () => {
  let component: GraphiquePrevisionComponent;
  let fixture: ComponentFixture<GraphiquePrevisionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraphiquePrevisionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphiquePrevisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
