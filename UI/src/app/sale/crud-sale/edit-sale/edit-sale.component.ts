import { Component, OnInit, Inject } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Sale} from 'src/app/template/classes/sales';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-edit-sale',
  templateUrl: './edit-sale.component.html',
  styleUrls: ['./edit-sale.component.css']
})
export class EditSaleComponent implements OnInit {

  Editform;
  dataSale: any = [];
  id: number;

  constructor(private api: ApiService, public dialog: MatDialog,
  @Inject(MAT_DIALOG_DATA) public data: {
    id: number;
  }) { 
    this.id = data.id;
  }
  ngOnInit(): void {
    // //Affiche tous les produits
    this.api.getSaleId(this.id).subscribe(
      data=> {
        this.dataSale = data;
    });
   
    // Formulaire pour l'envoi des data à l'api
    this.Editform = new FormGroup({
      client: new FormControl(this.dataSale.client),
      recettes: new FormControl(this.dataSale.recettes),
      date: new FormControl(this.dataSale.date),
      heure: new FormControl(this.dataSale.heure),
    });
  }

  // Envoi les data en format json
  onSubmit(client,recettes,date,heure){
    this.Editform.setValue({client:client, recettes:recettes, date:date, heure:heure });
    this.api.putSale(this.id,this.Editform.value).subscribe();
    console.log(this.Editform.value);
  }

}
