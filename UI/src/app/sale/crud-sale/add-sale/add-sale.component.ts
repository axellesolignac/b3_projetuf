import { Component, OnInit } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Customer} from 'src/app/template/classes/customers';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-add-sale',
  templateUrl: './add-sale.component.html',
  styleUrls: ['./add-sale.component.css']
})
export class AddSaleComponent implements OnInit {

  Addform;

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    // Formulaire pour l'envoi des data à l'api
    this.Addform = new FormGroup({
      client: new FormControl(),
      recettes: new FormControl(),
      date: new FormControl(),
      heure: new FormControl(),
    });
  }

  // Envoi les data en format json
  onSubmit(data){
    this.api.postSale(data).subscribe();
    console.log(data);
  }


}
