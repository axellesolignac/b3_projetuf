import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudSaleComponent } from './crud-sale.component';

describe('CrudSaleComponent', () => {
  let component: CrudSaleComponent;
  let fixture: ComponentFixture<CrudSaleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrudSaleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
