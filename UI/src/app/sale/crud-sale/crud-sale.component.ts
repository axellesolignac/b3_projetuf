import { Component, OnInit } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Sale} from 'src/app/template/classes/Sales';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material/dialog';
import {AddSaleComponent} from 'src/app/sale/crud-sale/add-sale/add-sale.component';
import {EditSaleComponent} from 'src/app/sale/crud-sale/edit-sale/edit-sale.component'


@Component({
  selector: 'app-crud-sale',
  templateUrl: './crud-sale.component.html',
  styleUrls: ['./crud-sale.component.css']
})
export class CrudSaleComponent implements OnInit {

  firstname: string;
  Addform;
  dataSale: any = [];
  // Variables pour la table
  dataSource = new MatTableDataSource<Sale>();
  displayedColumns: string[] = ['client', 'recettes', 'date', 'heure','Actions'];

  constructor(private api: ApiService, public dialog: MatDialog) {}

  ngOnInit(): void {
    //Affiche tous les Sales
    this.api.getSale().subscribe(
      data=> {
        this.dataSale = data;
        this.dataSource = new MatTableDataSource(this.dataSale);
    });
  }

  // Ouvre formulaire d'ajout
  openAdd(){
    const dialogRef = this.dialog.open(AddSaleComponent);
    dialogRef.afterClosed().subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Ouvre formulaire de modification en passant l'id du Sale
  openEdit(id){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id : id
    }
    const dialogRef = this.dialog.open(EditSaleComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Supprime le Sale de la ligne choisie
  onDelete(id){
    this.api.deleteSale(id).subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Fonction pour la recherche d'un Sale
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
