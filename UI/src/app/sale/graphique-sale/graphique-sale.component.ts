import { Component, OnInit } from '@angular/core';
import { ChartDataSets } from 'chart.js';
import { Label, Color } from 'ng2-charts';

@Component({
  selector: 'app-graphique-sale',
  templateUrl: './graphique-sale.component.html',
  styleUrls: ['./graphique-sale.component.css']
})
export class GraphiqueSaleComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  lineChartData: ChartDataSets[] = [
    { data: [45, 62, 78, 89, 68, 75], label: 'Ventes' },
  ];

  lineChartLabels: Label[] = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin'];

  lineChartOptions = {
    responsive: true,
  };

  lineChartColors: Color[] = [
    {
      borderColor: 'rgba(25,132,255,0.66)',
      backgroundColor: 'rgba(97,163,229,0.47)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';
}
