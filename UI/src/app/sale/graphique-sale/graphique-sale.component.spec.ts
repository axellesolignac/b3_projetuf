import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphiqueSaleComponent } from './graphique-sale.component';

describe('GraphiqueSaleComponent', () => {
  let component: GraphiqueSaleComponent;
  let fixture: ComponentFixture<GraphiqueSaleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraphiqueSaleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphiqueSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
