import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertesStockComponent } from './alertes-stock.component';

describe('AlertesStockComponent', () => {
  let component: AlertesStockComponent;
  let fixture: ComponentFixture<AlertesStockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlertesStockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertesStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
