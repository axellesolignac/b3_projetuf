import { Component, OnInit } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';

@Component({
  selector: 'app-alertes-stock',
  templateUrl: './alertes-stock.component.html',
  styleUrls: ['./alertes-stock.component.css']
})
export class AlertesStockComponent implements OnInit {

  data1;
  data2:any=[];

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.api.getStock().subscribe(element=>{
      this.data1 = element
      this.data1.forEach(item => {
        if(item.quantiteProduit<=5){
          this.data2.push({
            id:item.id,
            IDProduit:item.IDProduit,
            quantite:item.quantiteProduit
          })
        }      
      });
    })
  }

}
