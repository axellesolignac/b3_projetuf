import { Component, OnInit, Inject } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Stock} from 'src/app/template/classes/stocks';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-edit-stock',
  templateUrl: './edit-stock.component.html',
  styleUrls: ['./edit-stock.component.css']
})
export class EditStockComponent implements OnInit {

 
  Editform;
  dataStock: any = [];
  id: number;

  constructor(private api: ApiService, public dialog: MatDialog,
  @Inject(MAT_DIALOG_DATA) public data: {
    id: number;
  }) { 
    this.id = data.id;
  }
  ngOnInit(): void {
    // //Affiche tous le stock
    this.api.getStockId(this.id).subscribe(
      data=> {
        this.dataStock = data;
        console.log(this.id);
    });
   
    // Formulaire pour l'envoi des data à l'api
    this.Editform = new FormGroup({
      IDProduit: new FormControl(this.dataStock.IDProduit),
      quantiteProduit: new FormControl(this.dataStock.quantiteProduit),
    });
  }

  // Envoi les data en format json
  onSubmit(IDProduit,quantiteProduit){
    this.Editform.setValue({IDProduit:IDProduit, quantiteProduit:quantiteProduit });
    this.api.putStock(this.id,this.Editform.value).subscribe();
    // console.log(this.Editform.value);
  }

}
