import { Component, OnInit } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Stock} from 'src/app/template/classes/Stocks';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material/dialog';
import {AddStockComponent} from 'src/app/Stock/crud-Stock/add-Stock/add-Stock.component';
import {EditStockComponent} from 'src/app/Stock/crud-Stock/edit-Stock/edit-Stock.component'

@Component({
  selector: 'app-crud-stock',
  templateUrl: './crud-stock.component.html',
  styleUrls: ['./crud-stock.component.css']
})
export class CrudStockComponent implements OnInit {

  firstname: string;
  Addform;
  dataStock: any = [];
  dataProduct: any = [];
  data: any = [];
  // Variables pour la table
  dataSource = new MatTableDataSource<Stock>();
  displayedColumns: string[] = ['IDProduit','Produit', 'quantite','Actions'];

  constructor(private api: ApiService, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.data = []
    //Affiche tous les Stocks
    this.api.getStock().subscribe(
      data=> {
        this.dataStock = data;
        this.dataStock.forEach(element => {
          this.api.getProductId(element.IDProduit).subscribe(test=>{
            this.dataProduct = test;
            this.data.push(
              {
                id: element.id,
                IDProduit: element.IDProduit,
                intitule: this.dataProduct.intitule,
                quantiteProduit: element.quantiteProduit
              }
            );
            this.dataSource = new MatTableDataSource(this.data);
          });
        });
      });
  }

  // Ouvre formulaire d'ajout
  openAdd(){
    const dialogRef = this.dialog.open(AddStockComponent);
    dialogRef.afterClosed().subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Ouvre formulaire de modification en passant l'id du Stock
  openEdit(id){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id : id
    }
    const dialogRef = this.dialog.open(EditStockComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Supprime le Stock de la ligne choisie
  onDelete(id){
    this.api.deleteStock(id).subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Fonction pour la recherche d'un Stock
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
