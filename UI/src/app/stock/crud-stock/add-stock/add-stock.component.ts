import { Component, OnInit } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Recipe} from 'src/app/template/classes/recipes';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-add-stock',
  templateUrl: './add-stock.component.html',
  styleUrls: ['./add-stock.component.css']
})
export class AddStockComponent implements OnInit {

  Addform;

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    // Formulaire pour l'envoi des data à l'api
    this.Addform = new FormGroup({
      IDProduit: new FormControl(),
      quantiteProduit: new FormControl(),
    });
  }

  // Envoi les data en format json
  onSubmit(data){
    this.api.postStock(data).subscribe();
  }

}
