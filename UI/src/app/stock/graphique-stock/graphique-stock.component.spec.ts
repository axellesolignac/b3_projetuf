import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphiqueStockComponent } from './graphique-stock.component';

describe('GraphiqueStockComponent', () => {
  let component: GraphiqueStockComponent;
  let fixture: ComponentFixture<GraphiqueStockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraphiqueStockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphiqueStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
