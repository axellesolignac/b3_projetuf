import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import {ApiService} from 'src/app/template/services/api.service';

@Component({
  selector: 'app-graphique-stock',
  templateUrl: './graphique-stock.component.html',
  styleUrls: ['./graphique-stock.component.css']
})
export class GraphiqueStockComponent implements OnInit {
  firstname: string;
  Addform;
  dataStock: any = [];
  dataProduct: any = [];
  data: any = [];
  dataS:any=[];
  labelProduit:any = [];
  constructor(private api: ApiService) { }

  ngOnInit(): void {

    //Affiche tous les Stocks
    this.api.getStock().subscribe(
      data=> {
        this.dataStock = data;
        this.dataStock.forEach(element => {
          this.api.getProductId(element.IDProduit).subscribe(test=>{
            this.dataProduct = test;
            this.data.push(
              {
                id: element.id,
                IDProduit: element.IDProduit,
                intitule: this.dataProduct.intitule,
                quantiteProduit: element.quantiteProduit
              }
            );
            this.labelProduit.push(this.dataProduct.intitule);
            this.dataS.push(element.quantiteProduit);
          });
        });
      });
      this.barChartLabels = this.labelProduit;
  }
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  barChartData: ChartDataSets[] = [
    { data: this.dataS, label: 'KG', backgroundColor:'rgba(147,25,255,0.45)', borderColor:'rgba(147,25,255,0.74)' }
  ];
}
