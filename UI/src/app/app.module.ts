import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidenavComponent } from './template/sidenav/sidenav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AppRoutingModule } from './app-routing.module';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import {MatTableModule} from '@angular/material/table';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatTabsModule} from '@angular/material/tabs';
import { CustomerChoiceComponent } from './dashboard/customer-choice/customer-choice.component';
import { RecipeChoiceComponent } from './dashboard/recipe-choice/recipe-choice.component';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CustomerComponent } from './customer/customer.component';
import {MatDialogModule} from '@angular/material/dialog';
import { AddCustomerComponent } from './customer/add-customer/add-customer.component';
import { EditCustomerComponent } from './customer/edit-customer/edit-customer.component';
import { ProductComponent } from './product/product.component';
import { CrudComponent } from './product/crud-product/crud-product.component';
import { AddProductComponent } from './product/crud-product/add-product/add-product.component';
import { EditProductComponent } from './product/crud-product/edit-product/edit-product.component';
import { SaleComponent } from './sale/sale.component';
import { GraphiqueSaleComponent } from './sale/graphique-sale/graphique-sale.component';
import { CrudSaleComponent } from './sale/crud-sale/crud-sale.component';
import { AddSaleComponent } from './sale/crud-sale/add-sale/add-sale.component';
import { EditSaleComponent } from './sale/crud-sale/edit-sale/edit-sale.component';
import { GraphiqueProductComponent } from './product/graphique-product/graphique-product.component';
import { RecipeComponent } from './recipe/recipe.component';
import { StockComponent } from './stock/stock.component';
import { CrudRecipeComponent } from './recipe/crud-recipe/crud-recipe.component';
import { DetailRecipeComponent } from './recipe/detail-recipe/detail-recipe.component';
import { AddRecipeComponent } from './recipe/crud-recipe/add-recipe/add-recipe.component';
import { EditRecipeComponent } from './recipe/crud-recipe/edit-recipe/edit-recipe.component';
import { CrudStockComponent } from './stock/crud-stock/crud-stock.component';
import { AlertesStockComponent } from './stock/alertes-stock/alertes-stock.component';
import { GraphiqueStockComponent } from './stock/graphique-stock/graphique-stock.component';
import { AddStockComponent } from './stock/crud-stock/add-stock/add-stock.component';
import { EditStockComponent } from './stock/crud-stock/edit-stock/edit-stock.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MomentDateModule } from '@angular/material-moment-adapter';
import { ChartsModule } from 'ng2-charts';
import { GraphiquePrevisionComponent } from './dashboard/graphique-prevision/graphique-prevision.component';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
    DashboardComponent,
    CustomerChoiceComponent,
    RecipeChoiceComponent,
    CustomerComponent,
    AddCustomerComponent,
    EditCustomerComponent,
    ProductComponent,
    CrudComponent,
    AddProductComponent,
    EditProductComponent,
    SaleComponent,
    GraphiqueSaleComponent,
    CrudSaleComponent,
    AddSaleComponent,
    EditSaleComponent,
    GraphiqueProductComponent,
    RecipeComponent,
    StockComponent,
    CrudRecipeComponent,
    DetailRecipeComponent,
    AddRecipeComponent,
    EditRecipeComponent,
    CrudStockComponent,
    AlertesStockComponent,
    GraphiqueStockComponent,
    AddStockComponent,
    EditStockComponent,
    GraphiquePrevisionComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AppRoutingModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatTabsModule,
    MatChipsModule,
    MatAutocompleteModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MomentDateModule,
    ChartsModule,
  ],
  providers: [   
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
