import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphiqueProductComponent } from './graphique-product.component';

describe('GraphiqueProductComponent', () => {
  let component: GraphiqueProductComponent;
  let fixture: ComponentFixture<GraphiqueProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraphiqueProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphiqueProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
