import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import {ApiService} from 'src/app/template/services/api.service';

@Component({
  selector: 'app-graphique-product',
  templateUrl: './graphique-product.component.html',
  styleUrls: ['./graphique-product.component.css']
})
export class GraphiqueProductComponent implements OnInit {

  dataP: any = [];
  labelIntitule:any = [];
  dataQuantite:any=[];

  public barChartOptions: ChartOptions = {
    responsive: true
  };
  public barChartType: ChartType = 'horizontalBar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
    { data: this.dataQuantite, label: 'Quantitée en unité ', stack: 'a', backgroundColor:'rgba(147,25,255,0.45)', borderColor:'rgba(147,25,255,0.74)'}
  ];
  public barChartLabels: string[] = [];

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.api.getProduct().subscribe(
      data=> {
        this.dataP = data;
        this.dataP.forEach(element => {
          this.labelIntitule.push(element.intitule);
          this.dataQuantite.push(element.quantite);
        });
    });
    this.barChartLabels = this.labelIntitule;
  }
}
