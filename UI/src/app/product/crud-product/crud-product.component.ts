import { Component, OnInit } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Product} from 'src/app/template/classes/products';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material/dialog';
import {AddProductComponent} from 'src/app/product/crud-product/add-product/add-product.component';
import {EditProductComponent} from 'src/app/product/crud-product/edit-product/edit-product.component'

@Component({
  selector: 'app-crud-product',
  templateUrl: './crud-product.component.html',
  styleUrls: ['./crud-product.component.css']
})
export class CrudComponent implements OnInit {

  firstname: string;
  Addform;
  dataProduct: any = [];
  // Variables pour la table
  dataSource = new MatTableDataSource<Product>();
  displayedColumns: string[] = ['id','intitule', 'prix', 'datePeremption', 'dateLivre', 'quantite', 'codeBarre', 'origine','Actions'];

  constructor(private api: ApiService, public dialog: MatDialog) {}

  ngOnInit(): void {
    //Affiche tous les Products
    this.api.getProduct().subscribe(
      data=> {
        this.dataProduct = data;
        this.dataSource = new MatTableDataSource(this.dataProduct);
    });
  }

  // Ouvre formulaire d'ajout
  openAdd(){
    const dialogRef = this.dialog.open(AddProductComponent);
    dialogRef.afterClosed().subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Ouvre formulaire de modification en passant l'id du Product
  openEdit(id){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id : id
    }
    const dialogRef = this.dialog.open(EditProductComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Supprime le Product de la ligne choisie
  onDelete(id){
    this.api.deleteProduct(id).subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Fonction pour la recherche d'un Product
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
