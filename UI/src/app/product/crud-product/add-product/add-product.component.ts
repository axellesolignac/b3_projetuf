import { Component, OnInit } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Customer} from 'src/app/template/classes/customers';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MAT_DATE_FORMATS } from '@angular/material/core';
   
export const MY_DATE_FORMATS = {
    parse: {
      dateInput: 'YYYY-MM-DD',
    },
    display: {
      dateInput: 'YYYY-MM-DD',
      monthYearLabel: 'MMMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY'
    },
};

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css'],
  providers: [
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
  ]
})
export class AddProductComponent implements OnInit {

  Addform;

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    // Formulaire pour l'envoi des data à l'api
    this.Addform = new FormGroup({
      intitule: new FormControl(),
      prix: new FormControl(),
      datePeremption: new FormControl(),
      dateLivre: new FormControl(),
      quantite: new FormControl(),
      codeBarre: new FormControl(),
      origine: new FormControl(),
    });
  }

  // Envoi les data en format json
  onSubmit(intitule,prix,datePeremption,dateLivre,quantite,codeBarre,origine){
    this.Addform.setValue({intitule:intitule, prix:prix, datePeremption:datePeremption, dateLivre:dateLivre, quantite:quantite, codeBarre:codeBarre, origine:origine})
    this.api.postProduct(this.Addform.value).subscribe();
    // console.log(this.Addform.value);
  }

}
