import { Component, OnInit, Inject } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Customer} from 'src/app/template/classes/customers';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MAT_DATE_FORMATS } from '@angular/material/core';
   
export const MY_DATE_FORMATS = {
    parse: {
      dateInput: 'YYYY-MM-DD',
    },
    display: {
      dateInput: 'YYYY-MM-DD',
      monthYearLabel: 'MMMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY'
    },
};

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css'],
  providers: [
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
  ]
})
export class EditProductComponent implements OnInit {

  Editform;
  dataProduct: any = [];
  id: number;

  constructor(private api: ApiService, public dialog: MatDialog,
  @Inject(MAT_DIALOG_DATA) public data: {
    id: number;
  }) { 
    this.id = data.id;
  }
  ngOnInit(): void {
    // //Affiche tous les produits
    this.api.getProductId(this.id).subscribe(
      data=> {
        this.dataProduct = data;
    });
   
    // Formulaire pour l'envoi des data à l'api
    this.Editform = new FormGroup({
      intitule: new FormControl(this.dataProduct.intitule),
      prix: new FormControl(this.dataProduct.prix),
      datePeremption: new FormControl(this.dataProduct.datePeremption),
      dateLivre: new FormControl(this.dataProduct.dateLivre),
      quantite: new FormControl(this.dataProduct.quantite),
      codeBarre: new FormControl(this.dataProduct.codeBarre),
      origine: new FormControl(this.dataProduct.origine),
    });
  }

  // Envoi les data en format json
  onSubmit(intitule,prix,datePeremption,dateLivre,quantite,codeBarre,origine){
    this.Editform.setValue({intitule: intitule, prix: prix, datePeremption: datePeremption, dateLivre: dateLivre, quantite: quantite, codeBarre: codeBarre, origine: origine, });
    this.api.putProduct(this.id,this.Editform.value).subscribe();
    // console.log(this.Editform.value);
  }


}
