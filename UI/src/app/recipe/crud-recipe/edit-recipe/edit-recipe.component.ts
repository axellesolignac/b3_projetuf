import { Component, OnInit, Inject } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Customer} from 'src/app/template/classes/customers';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-edit-recipe',
  templateUrl: './edit-recipe.component.html',
  styleUrls: ['./edit-recipe.component.css']
})
export class EditRecipeComponent implements OnInit {

  Editform;
  dataRecipe: any = [];
  id: number;

  constructor(private api: ApiService, public dialog: MatDialog,
  @Inject(MAT_DIALOG_DATA) public data: {
    id: number;
  }) { 
    this.id = data.id;
  }
  ngOnInit(): void {
    // //Affiche tous les produits
    this.api.getRecipeId(this.id).subscribe(
      data=> {
        this.dataRecipe = data;
    });
   
    // Formulaire pour l'envoi des data à l'api
    this.Editform = new FormGroup({
      intitule: new FormControl(this.dataRecipe.intitule),
      prix: new FormControl(this.dataRecipe.prix),
      ingredients: new FormGroup({
        IDProduit:new FormControl(this.dataRecipe.ingredients.IDProduit),
        quantite:new FormControl(this.dataRecipe.ingredients.quantite),
      }),
    });
  }

  // Envoi les data en format json
  onSubmit(data){
    // this.Editform.setValue({intitule: intitule, prix: prix, ingredient:[{IDProduit:ID,quantite:quantite}] });
    this.api.putRecipe(this.id,data).subscribe();
    // console.log(this.Editform.value);
  }

}
