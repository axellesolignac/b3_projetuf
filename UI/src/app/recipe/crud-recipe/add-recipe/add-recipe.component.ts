import { Component, OnInit } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Recipe} from 'src/app/template/classes/recipes';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-add-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.css']
})
export class AddRecipeComponent implements OnInit {

  Addform;
  Ingform;
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    // Formulaire pour l'envoi des data à l'api
    this.Addform = new FormGroup({
      intitule: new FormControl('',[Validators.required]),
      prix: new FormControl('',[Validators.required]),
      ingredients: new FormGroup({
        IDProduit:new FormControl('',[Validators.required]),
        quantite:new FormControl('',[Validators.required]),
      }),
    });
  }

  // // Envoi les data en format json
  // onSubmit(data){
  //   this.api.postRecipe(data).subscribe();
  // }

  // Envoi les data en format json
  onSubmit(data){
    // this.Addform.setValue({intitule: data.intitule, prix: data.prix, ingredients:[{IDProduit:data.IDProduit, quantite:data.quantite}] })
    this.api.postRecipe(data).subscribe();
  }

}
