import { Component, OnInit } from '@angular/core';
import {ApiService} from 'src/app/template/services/api.service';
import { FormGroup, FormControl } from '@angular/forms';
import {Recipe} from 'src/app/template/classes/Recipes';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material/dialog';
import {AddRecipeComponent} from 'src/app/Recipe/crud-recipe/add-recipe/add-recipe.component';
import {EditRecipeComponent} from 'src/app/Recipe/crud-recipe/edit-recipe/edit-recipe.component'
import { Observable } from 'rxjs';

@Component({
  selector: 'app-crud-recipe',
  templateUrl: './crud-recipe.component.html',
  styleUrls: ['./crud-recipe.component.css']
})
export class CrudRecipeComponent implements OnInit {

  firstname: string;
  Addform;
  dataRecipe: any = [];
  test:any = [];
  map = new Map();
  // Variables pour la table
  dataSource = new MatTableDataSource<Recipe[]>();
  displayedColumns: string[] = ['id', 'intitule', 'prix','compose','Actions'];

  constructor(private api: ApiService, public dialog: MatDialog) {}

  ngOnInit() {
    //Affiche tous les Recipes
    this.api.getRecipe().subscribe(
      data=> {
        this.dataRecipe = data;
        this.dataSource = new MatTableDataSource(this.dataRecipe);
    });
  }

  // Ouvre formulaire d'ajout
  openAdd(){
    const dialogRef = this.dialog.open(AddRecipeComponent);
    dialogRef.afterClosed().subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Ouvre formulaire de modification en passant l'id du Recipe
  openEdit(id){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      id : id
    }
    const dialogRef = this.dialog.open(EditRecipeComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Supprime le Recipe de la ligne choisie
  onDelete(id){
    this.api.deleteRecipe(id).subscribe(result=>{
      this.ngOnInit();
    });
  }
  // Fonction pour la recherche d'un Recipe
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}